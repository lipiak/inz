
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "curand_kernel.h"

#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#include <math.h>
#include <Windows.h>
#include <string>
#include <iostream>

#include "FunkcjePomocnicze.h"

__global__ void CUDAZliczWagiRodzicow(int * d_wagi_rodzicow, int * rodzice, int * d_tabela_wag, int * liczba_elementow)
{
	int suma = 0;
	int numer_wierzcholka;

	int * wskaznik_na_rodzica = rodzice + threadIdx.x * *liczba_elementow;

	for (int i = 0; i < *liczba_elementow; i++)
	{
		numer_wierzcholka = wskaznik_na_rodzica[i];
		suma += *((d_tabela_wag + i * *liczba_elementow) + numer_wierzcholka);
	}

	d_wagi_rodzicow[threadIdx.x] = suma;
}

__global__ void CUDAsetupRandomGenerators(curandState * state, unsigned long seed)
{
	int id = threadIdx.x;
	curand_init ( seed, id, 0, &state[id] );
}


__global__ void CUDAGenerateCrossFlags(curandState *devStates, int *d_czy_krzyzowac)
{
	curandState localState;
	localState = devStates[threadIdx.x];
	d_czy_krzyzowac[threadIdx.x] = (curand_uniform(&localState) <= 0.7);
	devStates[threadIdx.x] = localState;
}

__global__ void CUDACrossover(int *d_id_rodzice_1, int *d_id_rodzice_2, int *d_rodzice, int *d_wagi, int *d_potomkowie, int *d_liczba_wierzcholkow, int *flagi_uzycia_wierzcholkow)
{
	int *rodzic_1 = d_rodzice + d_id_rodzice_1[threadIdx.x] * *d_liczba_wierzcholkow;
	int *rodzic_2 = d_rodzice + d_id_rodzice_2[threadIdx.x] * *d_liczba_wierzcholkow;
	int *potomek = d_potomkowie + d_id_rodzice_1[threadIdx.x] * *d_liczba_wierzcholkow;
	int *flagi = flagi_uzycia_wierzcholkow + d_id_rodzice_1[threadIdx.x] * *d_liczba_wierzcholkow;

	int waga1;
	int waga2;

	for (int i = 0; i < *d_liczba_wierzcholkow; i++)
	{

	
		waga1 = *((d_wagi + i * *d_liczba_wierzcholkow) + rodzic_1[i]);
		waga2 = *((d_wagi + i * *d_liczba_wierzcholkow) + rodzic_2[i]);


		/*zast�pienie warunku
	
		if(waga1>=waga2)
			nowy_wierzcho�ek = rodzic_2[threadIdx.x];
		else
			nowy_wierzcho�ek = rodzic_1[threadIdx.x];

		funkcj� liniow� y = ax+b aby nie dopu�ci� do rozjechania si� w�tk�w
		*/
		int nowy_wierzcholek = (rodzic_2[i] - rodzic_1[i])*(waga1 >= waga2) + rodzic_1[i];
	

		/* zast�pienie warunku

		if(flagi[nowy_wierzcholek] == 1)
			potomek[threadIdx.x] = -1;
		else
			potomek[threadIdx.x] = nowy_wierzcho�ek;

		*/
		potomek[i] = (-1 - nowy_wierzcholek)*(flagi[nowy_wierzcholek] == 1) + nowy_wierzcholek;

		flagi[nowy_wierzcholek] = 1;
	}
}

__global__ void CUDAMutation(curandState *devStates, int *d_potomkowie, int *d_liczba_wierzcholkow)
{
	curandState localState;
	int czy_mutowac;

	int *potomek = d_potomkowie + threadIdx.x * *d_liczba_wierzcholkow;
	int temp;
	int w1;
	int w2;

	localState = devStates[threadIdx.x];
	czy_mutowac = (curand_uniform(&localState) <= 0.2);
	devStates[threadIdx.x] = localState;

	//wylosuj tez wierzcholki, ktore ulegna muacji
	localState = devStates[threadIdx.x];
	w1 = curand_uniform(&localState) * *d_liczba_wierzcholkow - 1;
	w2 = curand_uniform(&localState) * *d_liczba_wierzcholkow - 1;
	devStates[threadIdx.x] = localState;

	//i teraz zamiast ifa, trzeba znowu y = ax + b
	
	//je�li zachodzi mutacja, to przpisz potomek[w1] do temp
	temp = czy_mutowac * potomek[w1];

	//teraz przepisz potomek[1] do potomek[0], jak nie zachodzi mutacja, to zostaw potomek[0]
	potomek[w1] = czy_mutowac * (potomek[w2] - potomek[w1]) + potomek[w1];

	//przepisz temp do potomek[1], a jak nie zachodzi mutacja, to zostaw potomek[1]
	potomek[w2] = czy_mutowac * (temp - potomek[w2]) + potomek[w2];
}

int main()
{
	//dane globalne
	

	//liczba wszystkich wierzcho�k�w
	int tmpm;
	std::cout<<"Podaj liczbe par do przetworzenia: ";
	std::cin>>tmpm;

	int * liczba_losowych_rozwiazan = (int*)malloc(sizeof(int));

	std::cout<<"\nPodaj liczbe losowych rozwiazan dla metody ewolucyjnej: ";
	std::cin>>*liczba_losowych_rozwiazan;

	int *liczba_pokolen = (int*)malloc(sizeof(int));
	std::cout<<"\nPodaj liczbe pokolen dla metody ewolucyjnej: ";
	std::cin>>*liczba_pokolen;
	
	int m = tmpm;


	//liczba par oraz wierzcholkow w jednym rozwiazaniu
	int n = m*2;

	//tablica przechowujaca elementy lewe
	int *lewe = (int*)malloc(sizeof(int) * m);

	//tablica przechowujaca elementy prawe
	int *prawe = (int*)malloc(sizeof(int) * m);

	//wagi po��cze� mi�dzy poszczeg�lnymi wierzcho�kami z lewej i prawej tablicy
	int *wagi = (int*)malloc(sizeof(int) *m *m);

	//liczba rozwi�za� do sprawdzenia w metodzie referencyjnej
	__int64 *liczba_rozwiazan = (__int64*)malloc(sizeof(__int64));
	Silnia(m, liczba_rozwiazan);

	//rozmieszczanie losowe elementow miedzy tablicami
	//iteracja po ka�dym elemencie tablic
	//losowanie liczby losowej z zakresu 0-99
	//je�li wypadnie liczba parzysta, to dany element l�duje w lewej tablicy, je�li nie, to w prawej
	//nale�y pami�ta� o ograniczonej pojemno�ci tablic

	int p_l = 0; //aktualna liczba wierzcho�k�w w tablicy lewej
	int p_r = 0; //aktualna liczba wierzcho�k�w w tablicy prawej
	srand(time(0)); //uzale�nienie zmiennej losowej od czasu
	int r;
	for (int i = 0; i < n; i++)
	{
		r = rand() % 100;
		
		//je�li random parzysty i jest miejsce w tablicy
		//albo random nieparzysty i nie ma miejsca w tablicy
		if((r%2 == 0 && p_l < m) || (r%2 != 0 && p_r == m))
		{
			lewe[p_l] = i;
			p_l++;
			continue;
		}
		
		//je�li random nieparzysty i jest miejsce w tablicy
		//albo je�li random parzysty i nie ma miejsca w tablicy
		if((r%2 != 0 && p_r < m) || (r%2 == 0 && p_l == m))
		{
			prawe[p_r] = i;
			p_r++;
			continue;
		}

	}

	
	//generowanie wag z przedzia�u 1 - 100
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < m; j++)
		{
			*((wagi + i *m) + j) = rand() % 100 + 1;
		}
	}

	__int64 miejsca_w_tablicy = *liczba_rozwiazan * m;

	printf("\n\nRezerwowanie pamieci dla %d liczb w tablicy\n", miejsca_w_tablicy);
	int *wyniki_permutacji = (int*)malloc(sizeof(int) * miejsca_w_tablicy);
	//ZeroMemory(wyniki_permutacji, sizeof(int) * liczba_rozwiazan * m);
	__int64 numer_rozwiazanie = 0;

	//rozwiazanie o najlepszej wartosci
	//numer rozwiazania o najlepszej wartosci
	__int64 * min_waga = (__int64*)malloc(sizeof(__int64));
	__int64 * numer_min_rozwiazania = (__int64*)malloc(sizeof(__int64));
	__int64* wagi_wszystkich_rozwiazan = (__int64*)malloc(sizeof(__int64)* *liczba_rozwiazan);
	__int64 ilosc_wag_bez_duplikatow = 0;

	*min_waga = _I64_MAX;

	int obliczone_size = 0;
	int poziom = 0;

	

	printf("Liczba rozwiazan: %d\nTrwa ich obliczanie\n\n", *liczba_rozwiazan);

	clock_t start = clock();
		
		
	LiczPermutacje((int*)&m, &poziom, new int[0], &obliczone_size, wyniki_permutacji, &numer_rozwiazanie);
	ZnajdzNajlepszeRozwiazanie(numer_min_rozwiazania, min_waga, liczba_rozwiazan, m, wagi, wyniki_permutacji, lewe, prawe, wagi_wszystkich_rozwiazan);


	//wypisanie najlepszego rozwiazania
	printf("Wartosc najlepszego rozwiazania: %d\n", *min_waga);
	for (int i = 0; i < m; i++)
	{
		//printf("%d %d\n", lewe[i], prawe[*((wyniki_permutacji + m * *numer_min_rozwiazania) + i)]);
		printf("%d %d\n", i, *((wyniki_permutacji + m * *numer_min_rozwiazania) + i));
	}
		
	clock_t stop = clock();
	printf("Czas wykonywania algorytmu: %.20lfs", (long)(stop-start)/1000.0f);

	free(wyniki_permutacji);
	//wypisanie wynikow
	/*for (int i = 0; i < *liczba_rozwiazan; i++)
	{
		for (int j = 0; j < m; j++)
		{
			printf("%d %d\n",j, *((wyniki_permutacji + m * i) + j));
		}
		printf("\n");
	}*/

	//jeszcze posortowac malejaco wagi z metody referencyjnej
	printf("\n\nSortowanie wag metody referencyjnej dla ulatwienia pozniejszej oceny\n");
	//SortujMalejacoINT64(wagi_wszystkich_rozwiazan, *liczba_rozwiazan);
	quick_alg(0, *liczba_rozwiazan - 1, wagi_wszystkich_rozwiazan);
	wagi_wszystkich_rozwiazan = UsunDuplikaty(wagi_wszystkich_rozwiazan, &ilosc_wag_bez_duplikatow, liczba_rozwiazan);
	


	printf("\nWykonywanie algorytmu ewolucyjnego\n");
	// ========================================= ALGORYTM GENETYCZNY =============================================

	//inicjalizacja wszystkich potrzebnych zmiennych i tablic

	//1. Wylosowanie potencjalnych rodzic�w - tylko 1 raz
	

	
	int * rodzice = (int*)malloc(sizeof(int) * *liczba_losowych_rozwiazan * m);
	generateRandomNumbers(liczba_losowych_rozwiazan, rodzice, &m);

	//2. Zliczenie sumarycznych wag wszystkich wylosowanych rodzic�w
	int * wagi_rodzicow = (int*)malloc(*liczba_losowych_rozwiazan * sizeof(int));
	
	int * d_wagi_rodzicow = 0;
	cudaMalloc(&d_wagi_rodzicow, sizeof(int) * *liczba_losowych_rozwiazan);
	
	int * d_tabela_wag = 0;
	cudaMalloc(&d_tabela_wag, sizeof(int) * m * m);
	
	int * d_rodzice = 0;
	cudaMalloc(&d_rodzice, sizeof(int) * *liczba_losowych_rozwiazan * m);

	int * d_liczba_elementow = 0;
	cudaMalloc(&d_liczba_elementow, sizeof(int));

	int * wagi_posortowane = (int*)malloc(sizeof(int) * *liczba_losowych_rozwiazan);
	
	
	//tablica przechowuj�ca jaki procent powierzchni ko�a zajmuje dane rozwi�zanie
	float *pola = (float*)malloc(sizeof(float) * *liczba_losowych_rozwiazan);

	//przedzia�y wycink�w ko�a dla rozwi�za�
	float * dolna_granica = (float*)malloc(sizeof(float) * *liczba_losowych_rozwiazan);
	float * gorna_granica = (float*)malloc(sizeof(float) * *liczba_losowych_rozwiazan);

	//tablica, ktora mapuje wyliczone "powierzchnie" z konkretnymi rozwi�zaniami
	int * mapowanie_przedzialow = (int*)malloc(sizeof(int) * *liczba_losowych_rozwiazan);


	curandState *devStates;
	cudaMalloc(&devStates, *liczba_losowych_rozwiazan * sizeof(curandState));

	int *czy_krzyzowac = (int*)malloc(sizeof(int) * *liczba_losowych_rozwiazan);
	
	int *d_czy_krzyzowac;
	cudaMalloc(&d_czy_krzyzowac, sizeof(int) * *liczba_losowych_rozwiazan);

	int ile_do_krzyzowania = 0;
	int ile_bez_krzyzowania = 0;

	int *rodzice_do_krzyzowania_1 = 0;//(int*)malloc(sizeof(int) * ile_do_krzyzowania);
	int *rodzice_do_krzyzowania_2 = 0;//(int*)malloc(sizeof(int) * ile_do_krzyzowania);

	int *rodzice_bez_krzyzowania = 0;//(int*)malloc(sizeof(int) * ile_bez_krzyzowania);

	int maly_licznik_krzyzowanie = 0;
	int maly_licznik_bez_krzyzowania = 0;

	int *d_id_rodzice_1 = 0;
	//cudaMalloc(&d_id_rodzice_1, sizeof(int) * ile_do_krzyzowania);

	int *d_id_rodzice_2 = 0;
	//cudaMalloc(&d_id_rodzice_2, sizeof(int) * ile_do_krzyzowania);

	int *d_potomkowie;
	cudaMalloc(&d_potomkowie, sizeof(int) * *liczba_losowych_rozwiazan * m);

	int *flagi_uzycia_wierzcholkow = (int*)malloc(sizeof(int) * *liczba_losowych_rozwiazan *m);

	int *d_flagi_uzycia_wierzcholkow;
	cudaMalloc(&d_flagi_uzycia_wierzcholkow, sizeof(int) * *liczba_losowych_rozwiazan * m);

	int *potomkowie = (int*)malloc(sizeof(int) * *liczba_losowych_rozwiazan * m);

	int nr_rozwiazania = 0;

	int *id_rodzice_po_selekcji = (int*)malloc(sizeof(int) * *liczba_losowych_rozwiazan);

	int *wyselekcjonowani_rodzice = (int*)malloc(sizeof(int) * *liczba_losowych_rozwiazan * m);

	int *d_wyselekcjonowani_rodzice;
	cudaMalloc(&d_wyselekcjonowani_rodzice, sizeof(int) * *liczba_losowych_rozwiazan * m);

	

	start = clock();
	//to wystarczy zrobi� tylko raz
	cudaMemcpy(d_tabela_wag, wagi, sizeof(int) * m * m, cudaMemcpyHostToDevice);
	cudaMemcpy(d_rodzice, rodzice, sizeof(int) * *liczba_losowych_rozwiazan * m, cudaMemcpyHostToDevice);
	cudaMemcpy(d_liczba_elementow, &m, sizeof(int), cudaMemcpyHostToDevice);

	for (int populacja = 0; populacja < *liczba_pokolen; populacja++)
	{
		CUDAZliczWagiRodzicow<<<1, *liczba_losowych_rozwiazan>>>(d_wagi_rodzicow, d_rodzice, d_tabela_wag, d_liczba_elementow);

		cudaMemcpy(wagi_rodzicow, d_wagi_rodzicow, sizeof(int) * *liczba_losowych_rozwiazan, cudaMemcpyDeviceToHost);

		//cudaFree(d_tabela_wag);
		//cudaFree(d_liczba_elementow);
		//cudaFree(d_wagi_rodzicow);
		//cudaFree(d_rodzice);

	

	
		//3. Posortowanie wag rodzic�w w celu nadania im wsp�czynnik�w okre�laj�cych jak� powierzchni� "ko�a rozwi�za�" b�d� one zajmowa�y
	
		memcpy(wagi_posortowane, wagi_rodzicow, sizeof(int)* *liczba_losowych_rozwiazan);
		SortujMalejaco(wagi_posortowane, *liczba_losowych_rozwiazan);

	

		ObliczPola(wagi_posortowane, pola, *liczba_losowych_rozwiazan);

		//teraz nalezy wyliczyc przedzialy dla kazdego rozwiazania
	

		//wypelnienie tablic przedzialow
		dolna_granica[0] = 0;
		gorna_granica[0] = pola[0];
		for (int i = 1; i < *liczba_losowych_rozwiazan; i++)
		{
			dolna_granica[i] = gorna_granica[i-1];
			gorna_granica[i] = dolna_granica[i] + pola[i];
		}
		gorna_granica[*liczba_losowych_rozwiazan - 1] = 1.0f;

	
		MapujRozwiazaniaDoPrzedzialow(wagi_rodzicow, wagi_posortowane, mapowanie_przedzialow, liczba_losowych_rozwiazan);


	
		/*
		Ka�de pokolenie musi sk�ada� si� z takiej samej liczby osobnik�w.
		Ka�dy w�tek b�dzie reprezentowa� jednego rodzica.

		Ka�dy w�tek b�dzie dzia�a� nast�puj�co:
		- Do pami�ci wsp�dzielonej zostan� za�adowani rodzice, oraz ich przedzia�y na kole
		- Zostanie wylosowanie prawdopodobie�stwo krzy�owania (P=0.7).
		- Je�li krzy�owanie si� odb�dzie, z ko�a zostanie wylosowany drugi rodzic.
		- Je�li do krzy�owania zostan� wytypowane te same dwa rozwi�zania, w�wczas potomkiem zostaje rodzic reprezentowany przez w�tek.
		- Nowe rozwi�zanie otrzyma "lepsze" cechy rodzic�w, tj je�li po��czenie 0-0 pierwszego rodzica jest lepsze ni� np.
		  0-3 drugiego rodzica, potomek otrzyma po��czenie 0-0.
		- Je�li wyczerpi� si� mo�liwo�ci krzy�owania, wolne wierzcho�ki zostan� po��czone losowo
		- Na potomku zostanie wylosowana mo�liwo�� mutacji (P=0.2)
		- Je�li do mutacji dojdzie, dwa losowe wierzcho�ki zamieni� si� miejscami, np 0 1 2 3 -> 1 0 2 3
		- Koniec pracy w�tku
		*/

	

		//dokonanie selekcji - wylosowanie nowych potencjalnych rodzic�w z ko�a
		srand(time(0));
		float pValue;

		for (int i = 0; i < *liczba_losowych_rozwiazan; i++)
		{
			pValue = (rand()%100 + 1.0f)/100.0f; 

			for (int j = 0; j < *liczba_losowych_rozwiazan; j++)
			{
				if(pValue >= dolna_granica[j] && pValue <= gorna_granica[j])
					id_rodzice_po_selekcji[i] = mapowanie_przedzialow[j];
			}
		}

		//zamiana aktualnych rodzicow z tymi wyselekcjonowanymi
		int nr_rodzica;
		for (int i = 0; i < *liczba_losowych_rozwiazan; i++)
		{
			nr_rodzica = id_rodzice_po_selekcji[i];
			for (int j = 0; j < m; j++)
			{
				*((wyselekcjonowani_rodzice + i * m) + j) =  *((rodzice + nr_rodzica * m) + j);
			}
		}
		cudaMemcpy(d_rodzice, wyselekcjonowani_rodzice, sizeof(int) * *liczba_losowych_rozwiazan * m, cudaMemcpyHostToDevice);
	
	
		CUDAsetupRandomGenerators<<<1, *liczba_losowych_rozwiazan>>>(devStates, time(0));
		CUDAGenerateCrossFlags<<<1, *liczba_losowych_rozwiazan>>>(devStates, d_czy_krzyzowac);

		cudaMemcpy(czy_krzyzowac, d_czy_krzyzowac, sizeof(int) * *liczba_losowych_rozwiazan, cudaMemcpyDeviceToHost);

		
		ile_do_krzyzowania = ile_bez_krzyzowania = 0;
		for (int i = 0; i < *liczba_losowych_rozwiazan; i++)
		{
			czy_krzyzowac[i] == 1 ? ile_do_krzyzowania++ : ile_bez_krzyzowania++;
		}

		if(rodzice_do_krzyzowania_1 != 0)
			rodzice_do_krzyzowania_1 = (int*)realloc(rodzice_do_krzyzowania_1, sizeof(int) * ile_do_krzyzowania);
		else
			rodzice_do_krzyzowania_1 = (int*)malloc(sizeof(int) * ile_do_krzyzowania);

		if(rodzice_do_krzyzowania_2 != 0)
			rodzice_do_krzyzowania_2 = (int*)realloc(rodzice_do_krzyzowania_2, sizeof(int) * ile_do_krzyzowania);
		else
			rodzice_do_krzyzowania_2 = (int*)malloc(sizeof(int) * ile_do_krzyzowania);
		
		
		if(rodzice_bez_krzyzowania != 0)
			rodzice_bez_krzyzowania = (int*)realloc(rodzice_bez_krzyzowania, sizeof(int) * ile_bez_krzyzowania);
		else
			rodzice_bez_krzyzowania = (int*)malloc(sizeof(int) * ile_bez_krzyzowania);

		//obliczyc numery rozwiazan, ktore zostana skrzyzowane i tych, ktore nie zostana skrzyzowane
		maly_licznik_bez_krzyzowania = maly_licznik_krzyzowanie = 0;	
		for (int i = 0; i < *liczba_losowych_rozwiazan; i++)
		{
			if(czy_krzyzowac[i] == 1)	
				rodzice_do_krzyzowania_1[maly_licznik_krzyzowanie++] = i;
			else
				rodzice_bez_krzyzowania[maly_licznik_bez_krzyzowania++] = i;
		}

		//wylosowac drugich rodzicow do skrzyzowania
		srand(time(0));
		int w;
		for (int i = 0; i < ile_do_krzyzowania; i++)
		{
			w = rand()% *liczba_losowych_rozwiazan;
			rodzice_do_krzyzowania_2[i] = w;
		}

	
		

		

		if(d_id_rodzice_1 != 0)
		{
			cudaFree(d_id_rodzice_1);
			cudaMalloc(&d_id_rodzice_1, sizeof(int) * ile_do_krzyzowania);
		}
		else
			cudaMalloc(&d_id_rodzice_1, sizeof(int) * ile_do_krzyzowania);
		
		if(d_id_rodzice_2 != 0)
		{
			cudaFree(d_id_rodzice_2);
			cudaMalloc(&d_id_rodzice_2, sizeof(int) * ile_do_krzyzowania);
		}
		else
			cudaMalloc(&d_id_rodzice_2, sizeof(int) * ile_do_krzyzowania);
		

		cudaMemcpy(d_id_rodzice_1, rodzice_do_krzyzowania_1, sizeof(int) * ile_do_krzyzowania, cudaMemcpyHostToDevice);
		cudaMemcpy(d_id_rodzice_2, rodzice_do_krzyzowania_2, sizeof(int) * ile_do_krzyzowania, cudaMemcpyHostToDevice);
		cudaMemset(d_flagi_uzycia_wierzcholkow, 0, sizeof(int) * *liczba_losowych_rozwiazan * m);

		CUDACrossover<<<1, ile_do_krzyzowania>>>(d_id_rodzice_1, d_id_rodzice_2, d_rodzice, d_tabela_wag, d_potomkowie, d_liczba_elementow, d_flagi_uzycia_wierzcholkow);

		cudaMemcpy(potomkowie, d_potomkowie, sizeof(int) * *liczba_losowych_rozwiazan * m, cudaMemcpyDeviceToHost);
		cudaMemcpy(flagi_uzycia_wierzcholkow, d_flagi_uzycia_wierzcholkow, sizeof(int)* *liczba_losowych_rozwiazan *m, cudaMemcpyDeviceToHost);

		//CUDAGeneticModification<<<1, *liczba_losowych_rozwiazan>>>(devStates, d_rodzice, d_potomkowie, d_dolna_granica, 
																	//d_gorna_granica, d_mapowanie_przedzialow, d_liczba_losowych_rozwiazan, 
																	//d_liczba_elementow, d_tabela_wag);

		//w miejscach, gdzie jest -1, nale�y "rozwi�za� konflikt"
	
		for (int i = 0; i < ile_do_krzyzowania; i++)
		{
			nr_rozwiazania = rodzice_do_krzyzowania_1[i];
			int *potomek = potomkowie + nr_rozwiazania * m;
			int *flagi = flagi_uzycia_wierzcholkow + nr_rozwiazania * m;
		
			for (int j = 0; j < m; j++)
			{
				if(potomek[j] != -1)
					continue;

				for (int k = 0; k < m; k++)
				{
					//jesli wierzcholek k jest niewykorzystany dotychczas w rozwiazaniu, to go wpisz zamiast -1
					if(flagi[k] != 1)
					{
						potomek[j] = k;
						flagi[k] = 1;
						break;
					}
				}
			}

		}

		//przepisanie rozwiazan ktore nie zostaly skrzyzowane
		for (int i = 0; i < *liczba_losowych_rozwiazan; i++)
		{
			int *rodzic = wyselekcjonowani_rodzice + i * m;
			int *potomek = potomkowie + i * m;
			if(czy_krzyzowac[i] != 1)
			{
				for (int j = 0; j < m; j++)
					potomek[j] = rodzic[j];
			}
		}


		//mutowanie
		cudaMemcpy(d_potomkowie, potomkowie, sizeof(int) * *liczba_losowych_rozwiazan * m, cudaMemcpyHostToDevice);
		CUDAMutation<<<1, *liczba_losowych_rozwiazan>>>(devStates, d_potomkowie, d_liczba_elementow);
		cudaMemcpy(d_rodzice, d_potomkowie, sizeof(int) * *liczba_losowych_rozwiazan * m, cudaMemcpyDeviceToDevice);
		cudaMemcpy(rodzice, d_potomkowie, sizeof(int) * *liczba_losowych_rozwiazan * m, cudaMemcpyDeviceToHost);
	}

	//od teraz potomkowie to s� nowi rodzice
	cudaMemcpy(potomkowie, d_potomkowie, sizeof(int) * *liczba_losowych_rozwiazan * m, cudaMemcpyDeviceToHost);

	//ZnajdzNajlepszeRozwiazanie(numer_min_rozwiazania, min_waga, (long long*)liczba_losowych_rozwiazan, m, *wagi, potomkowie, lewe, prawe);
	int wynik_ewolucji = ZnajdzNajlepszegoPotomka(liczba_losowych_rozwiazan, potomkowie, m, wagi);
	stop = clock();

	float poprawnosc = OcenPoprawnoscRozwiazania(wynik_ewolucji, wagi_wszystkich_rozwiazan, &ilosc_wag_bez_duplikatow);

	printf("\n\nWynik algorytmu ewolucyjnego: %d, co daje %.2lf%s", wynik_ewolucji, poprawnosc*100.0f, "%");
	printf("\nCzas wykonywania algorytmu: %.20lfs", (long)(stop-start)/1000.0f);

	//czyszczenie pami�ci

	free(liczba_losowych_rozwiazan);
	free(liczba_pokolen);
	free(rodzice);

	free(wagi_rodzicow);
	
	cudaFree(d_wagi_rodzicow);
	

	cudaFree(d_tabela_wag);
	
	cudaFree(d_rodzice);


	cudaFree(d_liczba_elementow);

	free(wagi_posortowane);
	
	free(pola);

	free(dolna_granica);
	free(gorna_granica);

	free(mapowanie_przedzialow);


	cudaFree(devStates);

	free(czy_krzyzowac);
	

	cudaFree(d_czy_krzyzowac);


	free(rodzice_do_krzyzowania_1);
	free(rodzice_do_krzyzowania_2);

	free(rodzice_bez_krzyzowania);

	cudaFree(d_id_rodzice_1);

	cudaFree(d_id_rodzice_2);

	cudaFree(d_potomkowie);

	free(flagi_uzycia_wierzcholkow);


	cudaFree(d_flagi_uzycia_wierzcholkow);

	free(potomkowie);

	free(id_rodzice_po_selekcji);
	free(wyselekcjonowani_rodzice);
	cudaFree(d_wyselekcjonowani_rodzice);

	free(wagi_wszystkich_rozwiazan);

	system("PAUSE>NUL");
	return 0;
}



