#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <ctime>
#include <random>


void Silnia(char m, __int64 *wynik)
{
	*wynik = 1;
	for (int i = 1; i <= m; i++)
	{
		*wynik = *wynik * i;
	}

	
}


int Min(int x, int y)
{
	return x > y ? x : y;
}

bool IsMemberOfArray(int *ar, int size, int x)
{
	for (int i = 0; i < size; i++)
	{
		if(ar[i] == x)
			return true;
	}

	return false;
}

void LiczPermutacje(int *liczba_elementow, int *poziom, int *obliczone, int *obliczone_size, int *wyniki_permutacji, __int64 *numer_rozwiazania)
{
	int *i = new int;
	
	for (*i = 0; *i < *liczba_elementow; *i+=1)
	{
		//jesli taki wierzcho�ek jest ju� w rozwi�zaniu, pomi�
		if(IsMemberOfArray(obliczone, *obliczone_size, *i))
			continue;


		int *nowe_obliczone = new int[*obliczone_size + 1];

		//skopiuj aktualne rozwi�zanie do nowej tablicy
		int *k = new int;
		for (*k = 0; *k < *obliczone_size; *k+=1)
		{
			nowe_obliczone[*k] = obliczone[*k];
		}
		delete k;

		//dodaj nowy element na koniec nowej_tablicy
		nowe_obliczone[*obliczone_size] = *i;

		//je�li to jest ju� ostatni element do rozwi�zania
		if(*poziom == *liczba_elementow - 1)
		{
			//to jest rozwi�zanie, zapisz je, podbij licznik rozwi�za� i opu�� funkcj�
			int *j = new int;
			for (*j = 0; *j < *liczba_elementow; *j+=1)
			{
				*((wyniki_permutacji + *liczba_elementow * *numer_rozwiazania) + *j) = nowe_obliczone[*j];
			}

			delete j;
			delete i;
			*numer_rozwiazania = *numer_rozwiazania + 1;

			return;
		}
		else
		{
			//stw�rzmy nowe zmienne, by mo�na by�o p�niej bez problemu je skasowa�
			int *nowa_liczba_elementow = new int; 
			*nowa_liczba_elementow = *liczba_elementow;

			int *nowy_poziom = new int;
			*nowy_poziom = *poziom + 1;

			int *nowe_obliczone_size = new int;
			*nowe_obliczone_size = *obliczone_size + 1;

			int *nowe_nowe_obliczone = new int[*nowe_obliczone_size];
			//*nowe_nowe_obliczone = *nowe_obliczone;
			memcpy(nowe_nowe_obliczone, nowe_obliczone, sizeof(int) * *nowe_obliczone_size);

			

			//int *nowe_obliczone = new int[*nowe_obliczone_size];
			

			LiczPermutacje(nowa_liczba_elementow, nowy_poziom, nowe_nowe_obliczone, nowe_obliczone_size, wyniki_permutacji, numer_rozwiazania);

			
			delete nowa_liczba_elementow;
			delete nowy_poziom;
			delete nowe_obliczone_size;
			delete[] nowe_nowe_obliczone;
		}
	}
	
	delete i;

}

void ZnajdzNajlepszeRozwiazanie(__int64 *numer_min_rozwiazania, __int64 *min_rozwiazanie, __int64 *liczba_rozwiazan, int m, int *wagi, int *wyniki_permutacji, int *lewe, int *prawe, __int64 *wagi_wszystkich_rozwiazan)
{
	int suma;
	int indeks_lewy;
	int indeks_prawy;
	for (int i = 0; i < *liczba_rozwiazan; i++)
	{
		suma = 0;
		for (int j = 0; j < m; j++)
		{
			indeks_lewy = j;
			indeks_prawy = *((wyniki_permutacji + m * i) + j);
			suma = suma + *((wagi + m * indeks_lewy) + indeks_prawy);
		}

		wagi_wszystkich_rozwiazan[i] = suma;

		if (suma < *min_rozwiazanie || suma == 0)
		{
			*min_rozwiazanie = suma;
			*numer_min_rozwiazania = i;
		}
	}
}

void generateRandomNumbers(int *liczba_rozwiazan, int *d_wyniki, const int *d_liczba_elementow)
{
	srand(time(0));
	//d_wyniki[ind] = curand_poisson(&localState, 5) % 5;

	//state[ind] = localState;
	int element = 0;
	int iteracja;
	int n; 
	int *wskaznik_na_wyniki;   //wskaze miejsce w tablicy wynikowej, w ktore bedzie zapisywa� dany w�tek

	std::default_random_engine generator;
	std::uniform_real_distribution<double> distribution(0.0,1.0);
	


	for (int k = 0; k < *liczba_rozwiazan; k++)
	{
		wskaznik_na_wyniki = d_wyniki + *d_liczba_elementow * k;
		n = *d_liczba_elementow;
		iteracja = 0;
		while(n > 0)
		{
		
			//wylosuj ktory element dojdzie do nowego rozwiazania
			//element = (int)ceil(distribution(generator) * n) - 1;
			element = rand()%n;

			//sprawdz czy jest w wyniku
			for (int i = 0; i < *d_liczba_elementow; i)
			{
				//jesli jest to podbij nr elementu o 1 i zacznij sprawdzac wynik od nowa
				if(wskaznik_na_wyniki[i] == element)
				{
					element++;
					i = 0;
					continue;
				}
				i++;
			}

			//jesli tu wyszlo, to znaczy ze znaleziony zostal element ktory dolaczy do rozwiazania
			wskaznik_na_wyniki[iteracja] = element;
			n--;
			iteracja++;
		}
	}

}


void SortujMalejaco(int tab[],int n)
{
  int pom;
 
  for(int i=0;i<n;i++)
    for(int j=0;j<n-i-1;j++) //p�tla wewn�trzna
    if(tab[j]<tab[j+1])
    {
      //zamiana miejscami
      pom = tab[j];
      tab[j] = tab[j+1];
      tab[j+1] = pom;
    }
}

void SortujMalejacoINT64(__int64 tab[], __int64 n)
{
  int pom;
 
  for(__int64 i=0;i<n;i++)
    for(int j=0;j<n-i-1;j++) //p�tla wewn�trzna
    if(tab[j]<tab[j+1])
    {
      //zamiana miejscami
      pom = tab[j];
      tab[j] = tab[j+1];
      tab[j+1] = pom;
    }
}


void ObliczPola(int *wagi_posortowane, float *pola, int m)
{
	pola[0] = 1;
	float suma = pola[0];

	int podbicie_wagi = 1;
	int aktualna_waga = 1;

	for (int i = 1; i < m; i++)
	{
		if(wagi_posortowane[i-1] == wagi_posortowane[i])
		{
			pola[i] = aktualna_waga;
			podbicie_wagi++;
		}
		else
		{
			aktualna_waga += podbicie_wagi;
			podbicie_wagi = 1;
			pola[i] = aktualna_waga;
		}
		suma += pola[i];
	}

	//obliczenie procentu powierzchni ko�a zajmowanej przez dane rozwiazanie
	for (int i = 0; i < m; i++)
	{
		pola[i] = pola[i] / (float)suma;
	}

}


void MapujRozwiazaniaDoPrzedzialow(int *wagi_rodzicow, int *wagi_posortowane, int *mapowanie_przedzialow, int *liczba_losowych_rozwiazan)
{
	int indeks_szukany;
	int waga_szukana;

	for (int i = 0; i < *liczba_losowych_rozwiazan; i++)
	{
		waga_szukana = wagi_posortowane[i];

		for (int j = 0; j < *liczba_losowych_rozwiazan; j++)
		{
			if(waga_szukana == wagi_rodzicow[j])
			{
				//znaleziona taka waga w pierwotnej tablicy, teraz trzeba wy�uska� jej indeks
				//nale�y te� wykluczy� powtarzanie si� rozwi�za�
				if(!IsMemberOfArray(mapowanie_przedzialow, *liczba_losowych_rozwiazan, j))
				{
					mapowanie_przedzialow[i] = j;
					break;
				}
			}
		}
	}
}

int ZnajdzNajlepszegoPotomka(int * n, int * potomkowie, int m, int *wagi)
{
	int wynik = -1;
	int wierzcholek;
	int waga;
	int suma_wag = 0;

	for (int i = 0; i < *n; i++)
	{
		suma_wag = 0;
		for (int j = 0; j < m; j++)
		{
			wierzcholek = *((potomkowie + i * m) + j);
			suma_wag += *((wagi + j * m) + wierzcholek);
		}

		if(suma_wag < wynik || wynik == -1)
			wynik = suma_wag;
	}
	return wynik;
}

float OcenPoprawnoscRozwiazania(int wynik_ewolucji, __int64 *wagi, __int64 *liczba_rozwiazan)
{
	for (__int64 i = 0; i < *liczba_rozwiazan; i++)
	{
		if(wynik_ewolucji == wagi[i])
			return (float)i/(float)(*liczba_rozwiazan - 1);
	}
}

__int64* UsunDuplikaty(__int64 *wszystkie_wagi, __int64 *nowa_ilosc_wag, __int64 *liczba_wszystkich_rozwiazan)
{
	__int64 *nowe_wagi = (__int64*)malloc(sizeof(__int64) * *liczba_wszystkich_rozwiazan);

	__int64 nowa_dlugosc = 1;

	nowe_wagi[0] = wszystkie_wagi[0];
	int temp = wszystkie_wagi[0];
	
	for (__int64 i = 1; i < *liczba_wszystkich_rozwiazan; i++)
	{
		if(wszystkie_wagi[i] != temp)
		{
			temp = wszystkie_wagi[i];
			nowe_wagi[nowa_dlugosc++] = wszystkie_wagi[i];
		}
	}

	
	*nowa_ilosc_wag = nowa_dlugosc;
	return (__int64*)realloc(nowe_wagi, sizeof(__int64) * nowa_dlugosc);
}



void quick_alg(int index_pierwszego, int index_ostatniego, __int64* tab){
  
     int i, i2, tmp, srodek;

     i = (index_pierwszego + index_ostatniego) / 2;
  
     srodek = tab[i];
     tab[i] = tab[index_ostatniego];
  
     for( i2 = index_pierwszego, i = index_pierwszego ; i < index_ostatniego; i++){
          if( tab[i] > srodek){
              tmp = tab[i];
              tab[i] = tab[i2];
              tab[i2] = tmp;
              i2++;
              }
          }
      
     tab[index_ostatniego] = tab[i2];
     tab[i2] = srodek;
  
     if( (i2 + 1) < index_ostatniego){
         quick_alg(i2 + 1, index_ostatniego, tab);    //rekurencyjne wykonanie quicksorta dla prawej partycji
         }  
  
     if( index_pierwszego < (i2 - 1)){
         quick_alg(index_pierwszego, (i2 - 1), tab);  //rekurencyjne wykonanie quicksorta dla lewej partycji
         }
     }